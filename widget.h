#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include "history.h"
#include "cezarcalc.h"

QT_BEGIN_NAMESPACE
class QPushButton;
class QLabel;
class QLineEdit;
class QTextEdit;
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

    QString latinAlphabet = "abcdefghijklmnopqrstuvwxyz";
    QString cyrillicAlphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
    int maxLineLength = 20;

    QThread* thread;
    CezarCalc* calc;
public slots:
    void convertText();
    void showHistory();
    void backConvertText();
    void inputTextChanged(const QString & text);
    void outputTextChanged(const QString & text);
    void asynchTextChanged(const QString & text);
    void asynchConvertText();
    void showAsynchText(QString line);

private:
    int getCezarChar(const QChar& x,const QString & alphabet) const;

    QLineEdit *inputLine;
    QLineEdit *outputLine;
    QLineEdit *backLine;
    QLineEdit *asynchLine;
    QLineEdit *asynchOutputLine;
    QPushButton *convertButton;
    QPushButton *backConvertButton;
    QPushButton *showHistoryButton;
    QPushButton *asynchConvertButton;

    History *history;

    QVector<HistoryItem> vect;
};
#endif // WIDGET_H
