#ifndef CEZARCALC_H
#define CEZARCALC_H

#include <QObject>

class CezarCalc : public QObject
{
    Q_OBJECT
public:
    explicit CezarCalc(QObject *parent = nullptr);

    int Delay = 5000;

    Q_INVOKABLE void asynchCalc(const QString &line,const QString & latinAlphabet, const QString & cyrillicAlphabet);
signals:
    void sendPosition(QString output);
};

#endif // CEZARCALC_H
