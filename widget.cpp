#include <QtWidgets>

#include "widget.h"



Widget::Widget(QWidget *parent)
    : QWidget(parent)
{
    QLabel *inputLabel = new QLabel(tr("Text to convert:"));
    inputLine = new QLineEdit;
    connect(inputLine, SIGNAL(textChanged(QString)), this, SLOT(inputTextChanged(QString)));

    convertButton = new QPushButton(tr("&Convert"));
    connect(convertButton, SIGNAL(clicked()), this, SLOT(convertText()));

    QLabel *outputLabel = new QLabel(tr("Converted text:"));
    outputLine = new QLineEdit;
    connect(outputLine, SIGNAL(textChanged(QString)), this, SLOT(outputTextChanged(QString)));

    QLabel *backLabel = new QLabel(tr("Back converted text:"));
    backLine = new QLineEdit;
    backLine->setReadOnly(true);

    backConvertButton = new QPushButton(tr("&Back convert"));
    connect(backConvertButton, SIGNAL(clicked()), this, SLOT(backConvertText()));

    showHistoryButton =  new QPushButton(tr("&History"));
    connect(showHistoryButton, SIGNAL(clicked()), this, SLOT(showHistory()));

    QLabel *asynchLabel = new QLabel(tr("Text for asynch test:"));
    asynchLine = new QLineEdit;
    connect(asynchLine, SIGNAL(textChanged(QString)), this, SLOT(asynchTextChanged(QString)));

    asynchConvertButton = new QPushButton(tr("&Asynch calculation"));
    connect(asynchConvertButton, SIGNAL(clicked()), this, SLOT(asynchConvertText()));

    QLabel *asynchOutputLabel = new QLabel(tr("Asynch converted:"));
    asynchOutputLine = new QLineEdit;
    asynchOutputLine->setReadOnly(true);

    QGridLayout *mainLayout = new QGridLayout;
    mainLayout->addWidget(inputLabel, 0, 0);
    mainLayout->addWidget(inputLine, 0, 1);
    mainLayout->addWidget(convertButton, 0, 2);

    mainLayout->addWidget(outputLabel, 1, 0);
    mainLayout->addWidget(outputLine, 1, 1);
    mainLayout->addWidget(backConvertButton, 1, 2);

    mainLayout->addWidget(backLabel, 2, 0);
    mainLayout->addWidget(backLine, 2, 1);
    mainLayout->addWidget(showHistoryButton, 2, 2);

    mainLayout->addWidget(asynchLabel, 3, 0);
    mainLayout->addWidget(asynchLine, 3, 1);
    mainLayout->addWidget(asynchConvertButton, 3, 2);

    mainLayout->addWidget(asynchOutputLabel, 4, 0);
    mainLayout->addWidget(asynchOutputLine, 4, 1);


    setLayout(mainLayout);
    setWindowTitle(tr("Cezar crypto example"));

    thread = new QThread();
    calc = new CezarCalc();
    calc->moveToThread(thread);
    connect(calc,SIGNAL(sendPosition(QString)),this,SLOT(showAsynchText(QString)));
    thread->start();
}

int Widget::getCezarChar(const QChar& x,const QString & alphabet) const
{
    size_t pos(-1);
    for (int i = 0; i < alphabet.size(); ++i)
    {
        if (x == alphabet[i]) pos = i;
    }
    return pos;
}

void Widget::asynchConvertText()
{
    QString inputText = asynchLine->text().toLower();
    QMetaObject::invokeMethod(calc, "asynchCalc",
                           Qt::QueuedConnection,
                           Q_ARG(QString, inputText),
                           Q_ARG(QString, latinAlphabet),
                           Q_ARG(QString, cyrillicAlphabet )
                           );
}

void Widget::showAsynchText(QString line)
{
    asynchOutputLine->setText(line);
}

void Widget::inputTextChanged(const QString & text)
{
    QString localText(text);
    if (localText.size() > maxLineLength)
    inputLine->setText(localText.chopped(localText.size() - maxLineLength));
}

void Widget::outputTextChanged(const QString & text)
{
    QString localText(text);
    if (localText.size() > maxLineLength)
    outputLine->setText(localText.chopped(localText.size() - maxLineLength));
}

void Widget::asynchTextChanged(const QString & text)
{
    QString localText(text);
    if (localText.size() > maxLineLength)
    asynchLine->setText(localText.chopped(localText.size() - maxLineLength));
}


void Widget::convertText()
{
    QString inputText = inputLine->text().toLower();
    QString outputText;

     for (int i = 0; i < inputText.size(); i++)
     {
        int j = getCezarChar(inputText.at(i), latinAlphabet);
        int k = getCezarChar(inputText.at(i), cyrillicAlphabet);

        if ( j != -1 && k == -1)
        {
            if (j < latinAlphabet.size()-1) outputText.append(latinAlphabet[j+1]);
            else outputText.append(latinAlphabet[0]);
        }
        else if ( j == -1 && k != -1)
        {
            if (k < cyrillicAlphabet.size()-1) outputText.append(cyrillicAlphabet[k+1]);
            else outputText.append(cyrillicAlphabet[0]);
        }
        else
        {
            outputText.append(inputText.at(i));
        }
     }

    outputLine->clear();
    outputLine->setText(outputText);

    vect.push_back(HistoryItem(QTime::currentTime().toString("hh:mm:ss"),inputText,outputText));
}

void Widget::backConvertText()
{
    QString inputText = outputLine->text().toLower();
    QString outputText;

    for (int i = 0; i < inputText.size(); i++)
    {
       int j = getCezarChar(inputText.at(i), latinAlphabet);
       int k = getCezarChar(inputText.at(i), cyrillicAlphabet);

       if ( j != -1 && k == -1)
       {
           if (j > 0) outputText.append(latinAlphabet[j-1]);
           else outputText.append(latinAlphabet[latinAlphabet.size()-1]);
       }
       else if ( j == -1 && k != -1)
       {
           if (k > 0) outputText.append(cyrillicAlphabet[k-1]);
           else outputText.append(cyrillicAlphabet[cyrillicAlphabet.size()-1]);
       }
       else
       {
           outputText.append(inputText.at(i));
       }
    }
    backLine->setText(outputText);

    vect.push_back(HistoryItem(QTime::currentTime().toString("hh:mm:ss"),inputText,outputText));
}

void Widget::showHistory()
{
    History* localHistory = new History(this, &vect);
    localHistory->setWindowModality(Qt::WindowModal);
    localHistory->show();
}

Widget::~Widget()
{
}



