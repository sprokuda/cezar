#ifndef HISTORY_H
#define HISTORY_H

#include <QDialog>
#include <QMultiMap>

struct HistoryItem
{
    QString time;
    QString input;
    QString output;
    HistoryItem(QString time = "", QString input = "", QString output = ""): time(time),input (input), output(output){};
};

class History : public QDialog
{
    Q_OBJECT
public:
    History(QWidget *parent = nullptr, QVector<HistoryItem> *vect = nullptr);

signals:

};

#endif // HISTORY_H
