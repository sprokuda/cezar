#include "cezarcalc.h"
#include <QTest>
CezarCalc::CezarCalc(QObject *parent) : QObject(parent)
{

}

void CezarCalc::asynchCalc(const QString &line,const QString & latinAlphabet, const QString & cyrillicAlphabet)
{
    QString outputText;

    auto getChar = [](const QChar& x, const QString alphabet)
    {
        size_t pos(-1);
        for (int i = 0; i < alphabet.size(); ++i)
        {
            if (x == alphabet[i]) pos = i;
        }
        return pos;
    };

    for (int i = 0; i < line.size(); i++)
    {
       int j = getChar(line.at(i), latinAlphabet);
       int k = getChar(line.at(i), cyrillicAlphabet);

       if ( j != -1 && k == -1)
       {
           if (j < latinAlphabet.size()-1) outputText.append(latinAlphabet[j+1]);
           else outputText.append(latinAlphabet[0]);
       }
       else if ( j == -1 && k != -1)
       {
           if (k < cyrillicAlphabet.size()-1) outputText.append(cyrillicAlphabet[k+1]);
           else outputText.append(cyrillicAlphabet[0]);
       }
       else
       {
           outputText.append(line.at(i));
       }
    }

    QTest::qSleep(Delay);

    emit sendPosition(outputText);
};
