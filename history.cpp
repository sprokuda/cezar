#include <QtWidgets>
#include <QMultiMap>
#include "history.h"

History::History(QWidget *parent, QVector<HistoryItem> *vect) : QDialog(parent)
{

    QTableWidget* tableWidget = new QTableWidget(vect->size(), 3, this);
    int row = 0;
    for (QVector<HistoryItem>::iterator i = vect->begin(); i != vect->end(); i++)
    {
        tableWidget->setItem(row, 0, new QTableWidgetItem(i->time));
        tableWidget->setItem(row, 1, new QTableWidgetItem(i->input));
        tableWidget->setItem(row, 2, new QTableWidgetItem(i->output));
        row++;
    }

    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(tableWidget);
    setLayout(layout);
//    this->resize(tableWidget->columnWidth(0)*3*1.2,tableWidget->rowHeight(0)*vect->size()*1.5);


    int width = tableWidget->columnWidth(0)*3 + tableWidget->verticalHeader()->width()*3;
    int height = tableWidget->rowHeight(0)*(vect->size()+1) + tableWidget->horizontalHeader()->height();

    this->setSizePolicy(QSizePolicy(QSizePolicy::MinimumExpanding,QSizePolicy::MinimumExpanding));
    this->resize(width,height);


}
